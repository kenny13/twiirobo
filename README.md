* Register amazon account : [Amazone aws](http://aws.amazon.com/)

* Generate access keys (certificates X.509) : [Key Gen](https://portal.aws.amazon.com/gp/aws/securityCredentials)

* Download  : [ec2-api-tools-1.5.5.0](http://aws.amazon.com/developertools/351)

* Set paths to certificates 

        set PATH=D:\Users\devtools\ec2-api-tools-1.5.5.0\bin;%PATH%
        set EC2_HOME=D:\Users\devtools\ec2-api-tools-1.5.5.0
        set EC2_PRIVATE_KEY=D:\Users\devtools\ec2-api-tools-1.5.5.0\cert\pk-4OZYDKK7SH3Z7AJGURT3THSKSDPZF47U.pem 	
        set EC2_CERT=D:\Users\devtools\ec2-api-tools-1.5.5.0\cert\cert-4OZYTKK7SHFZ7AJURTT3THSHSDPZF47U.pem
        set EC2_URL=https://ec2.eu-west-1.amazonaws.com

* Check certificates is set up:  

        ec2-describe-keypairs

* Create security group 

        ec2-create-group my_sec_group -d "sec group for twitter bot" 
        
* Allow access to web and ssh 

        ec2-authorize my_sec_group -P tcp -p 80 -s 0.0.0.0/0
        ec2-authorize my_sec_group -P tcp -p 22 -s 0.0.0.0/0

* Run instance (one micro instance in europa data center, ubuntu 12.04 LTS 64b):

        ec2-run-instances ami-e1e8d395 
            --instance-count 1 
            --group my_sec_group 
            --instance-type t1.micro 
            --region eu-west-1 
            --availability-zone eu-west-1a 
            --monitor 
            --verbose 
            --key oskarskeypair

* get isntance public IP:

        ec2-describe-instances

* connect instance using public IP:

        ssh -v -i D:\Users\devtools\ec2-api-tools-1.5.5.0\cert\oskarskeypair.pem
            ubuntu@ec2-54-247-21-999.eu-west-1.compute.amazonaws.com

* install neaded soft: 

        sudo apt-get install git
        sudo apt-get install maven 
 
* check java home  (set if nead):

        java -version
        env
        export JAVA_HOME=/usr

* clone project:

        git clone https://github.com/kenny13/TwiiRobo.git	

* run web server: 

        mvn jetty:run 

* See results:

        ec2-54-247-21-999.eu-west-1.compute.amazonaws.com/twitterBot/




      
	
	
	

	
	
	
	


