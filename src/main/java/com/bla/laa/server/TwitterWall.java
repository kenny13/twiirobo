package com.bla.laa.server;

import java.io.IOException;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.List;

import twitter4j.Query;
import twitter4j.QueryResult;
import twitter4j.Tweet;
import twitter4j.Twitter;
import twitter4j.TwitterException;
import twitter4j.TwitterFactory;
import twitter4j.conf.ConfigurationBuilder;

public class TwitterWall
        extends javax.servlet.http.HttpServlet
{
    private static final String keyWords[] = { "smscredit", "vivus", "labiedarbi", "4finance" };
	public static final String NEW_LINE = "\n";

	protected void doGet(javax.servlet.http.HttpServletRequest request, javax.servlet.http.HttpServletResponse response)
            throws javax.servlet.ServletException, IOException
    {

		PrintWriter writer = new PrintWriter(new OutputStreamWriter(response.getOutputStream(), "UTF8"), true);

        List<Tweet> uniqTweets = new ArrayList<Tweet>();
        for (String keyWord : keyWords) {
            List<Tweet> tweets = getTwetts(keyWord);
            for (Tweet tweet : tweets) {
                if (!containTweet(uniqTweets, tweet)) {
                    uniqTweets.add(tweet);
                }
            }
        }

		writer.write(getHeader());
        for (Tweet tweet : uniqTweets) {
            writer.write("@" + tweet.getFromUser() + " - " + tweet.getText() + "<br>");
        }
		writer.write(getFooter());
		writer.flush();
        response.setContentType("text/html;charset=UTF-8");

		response.setCharacterEncoding("UTF-8");
    }

    private List<Tweet> getTwetts(String searchString)
    {
        Twitter twitter = getTwitterInstance();
        List<Tweet> tweets = new ArrayList<Tweet>();
        try {
            QueryResult result = twitter.search(new Query(searchString));
            tweets.addAll(result.getTweets());
        } catch (TwitterException te) {
            te.printStackTrace();
            System.out.println("Failed to search tweets: " + te.getMessage());
        }
        return tweets;
    }

    private boolean containTweet(List<Tweet> tweets, Tweet tweet)
    {
        for (Tweet curTweet : tweets) {
            if (curTweet.getId() == tweet.getId())
                return true;
        }
        return false;

    }

    private Twitter getTwitterInstance()
    {
        ConfigurationBuilder cb = new ConfigurationBuilder();
        cb.setDebugEnabled(true).setOAuthConsumerKey("1sBj7jDo9bcnAFR5KX2VA")
                .setOAuthConsumerSecret("0dtcL3uApFybhGNvSBcwndBTL5b5PbJgYs32HRbmBA")
                .setOAuthAccessToken("621859626-lvWdtCZLQ0G3QwuxhTftoOtfsUmx6W5ELHMeXEh8")
                .setOAuthAccessTokenSecret("Z2HoJCLGwpGJ5rsgbxDdLCrgghKKSQ5tm42rz5xeNE");
        TwitterFactory tf = new TwitterFactory(cb.build());
        return tf.getInstance();
    }

    private String getHeader()
    {
        StringBuffer sb = new StringBuffer();
        sb.append("<!DOCTYPE HTML PUBLIC \"-//W3C//DTD HTML 4.01 Transitional//EN\"").append(NEW_LINE);
        sb.append("http://www.w3.org/TR/html4/loose.dtd\">").append(NEW_LINE);
        sb.append("<html>").append(NEW_LINE);
        sb.append("<head>").append(NEW_LINE);
		sb.append("<meta http-equiv=\"content-type\" content=\"text/html;charset=utf-8\"/>").append(NEW_LINE);
        sb.append("<title></title>").append(NEW_LINE);
        sb.append("</head>").append(NEW_LINE);
        sb.append("<body>").append(NEW_LINE);

		return sb.toString();
    }

    private String getFooter()
    {
        StringBuffer sb = new StringBuffer();
        sb.append("</body>").append(NEW_LINE);
        sb.append("</html>").append(NEW_LINE);

        return sb.toString();
    }

}
